import { defineConfig } from 'astro/config';
import partytown from "@astrojs/partytown";
import sitemap from '@astrojs/sitemap';
import robotsTxt from 'astro-robots-txt';
import compress from "astro-compress";

export default defineConfig({
  site: 'https://aryan.ink',
  integrations: [
				sitemap(),
				robotsTxt(), 
				compress(),
				partytown({config: {forward: ["datalayer.push"],},}),
				]
});