import rss from '@astrojs/rss';

export function get(context) {
  return rss({
    title: 'Aryan’s Homepage',
    description: 'Aryan is a comedian and writer based out of Calcutta, India.',
    site: context.site,
    items: [],
    customData: `<language>en-us</language>`,
  });
}